import pyarrow
import pyarrow.flight

host = "localhost"
port = 4242

scheme = "grpc+tcp"
connection_args = {}

client = pyarrow.flight.FlightClient.connect(f"{scheme}://{host}:{port}",
                                                **connection_args)

print('Flights\n=======')
for flight in client.list_flights():
    descriptor = flight.descriptor
    if descriptor.descriptor_type == pyarrow.flight.DescriptorType.PATH:
        print("Path:", descriptor.path)
    elif descriptor.descriptor_type == pyarrow.flight.DescriptorType.CMD:
        print("Command:", descriptor.command)
    else:
        print("Unknown descriptor type")

    print("Total records:", end=" ")
    if flight.total_records >= 0:
        print(flight.total_records)
    else:
        print("Unknown")

    print("Total bytes:", end=" ")
    if flight.total_bytes >= 0:
        print(flight.total_bytes)
    else:
        print("Unknown")

    print("Number of endpoints:", len(flight.endpoints))

    if args.list:
        print(flight.schema)

    print('---')

print('\nActions\n=======')
for action in client.list_actions():
    print("Type:", action.type)
    print("Description:", action.description)
    print('---')
