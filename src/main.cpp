// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//  Created by Mikhail Kalugin on 11 / 02 / 2019.
//  Copyright 2019 Mikhail Kalugin All rights reserved.
//
// This file is a part of Siglab project
//
// Grinder - Signal data processing and delvery server.
//
// Copyright(C) 2019 Mikhail Kalugin.
//
// This program is free software : you can redistribute it and /or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see < https://www.gnu.org/licenses/>.

// ���-���� ������.
// Still, actors.


/// \mainpage Grinder Index Page
///
/// \section intro_sec Introduction
///
/// This is the introduction.
///
/// \section install_sec Installation
///
/// \subsection step1 Step 1: Opening the box
///
/// etc...

#include <string>
#include <iostream>

#include <caf/all.hpp>
#include "flight/FlightServer.h"
#include <future>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>

#include "config.h"

using namespace std::string_literals;

using serve_atom = caf::atom_constant<caf::atom("serve")>;

using trace_atom = caf::atom_constant<caf::atom("trace")>;
using debug_atom = caf::atom_constant<caf::atom("debug")>;
using info_atom = caf::atom_constant<caf::atom("info")>;
using warning_atom = caf::atom_constant<caf::atom("warning")>;
using error_atom = caf::atom_constant<caf::atom("error")>;
using fatal_atom = caf::atom_constant<caf::atom("fatal")>;



caf::behavior logger_fun(caf::event_based_actor* self)
{
    return {
        [=](trace_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(trace) << what;
            return what;
        },
        [=](debug_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(debug) << what;
            return what;
        },
        [=](info_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(info) << what;
            return what;
        },
        [=](warning_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(warning) << what;
            return what;
        },
        [=](error_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(error) << what;
            return what;
        },
        [=](fatal_atom, const std::string& what) -> std::string {
            BOOST_LOG_TRIVIAL(fatal) << what;
            return what;
        }
    };
}

void server_fun(caf::blocking_actor* self, const caf::actor &logger)
{
    bool running = true;
    std::shared_ptr<FlightServer> server = std::make_shared<FlightServer>(self,logger);
    self->send(self, serve_atom::value);
    self->receive_while(running) (
        [&](serve_atom) {
            /// \todo ��������� ��� � config.in / config.h
            unsigned int port = DEFAULT_PORT;
            std::string host(DEFAULT_HOST);


            if (boost::filesystem::exists("config.ini")) {
                boost::property_tree::ptree pt;
                boost::property_tree::ini_parser::read_ini("config.ini", pt);

                host = pt.get<std::string>("Network.host");
                port = pt.get<unsigned int>("Network.port");
            }


            arrow::flight::Location loc = {};

            arrow::Status stat = arrow::flight::Location::ForGrpcTcp(host, port, &loc);
            if (!stat.ok()) {
                self->send(logger, fatal_atom::value, "Can not create a location instance");
                return;
            }
            arrow::flight::FlightServerOptions options(loc);

            stat = server->Init(options);
            if (!stat.ok()) {
                self->send(logger, fatal_atom::value, "Can not initialize server");
                return;
            }

            self->send(logger, info_atom::value, "Server initialized");
            stat = server->Serve();
            if (!stat.ok()) {
                self->send(logger, fatal_atom::value, "Server error");
                return;
            }
        },
        [&](caf::exit_msg& em) {
            if (em.reason) {
                server->Shutdown();
                self->fail_state(std::move(em.reason));
                running = false;
            }
        }
    );
}

void caf_main(caf::actor_system& system)
{
    auto logger = system.spawn(logger_fun);
    auto server = system.spawn(server_fun, logger);

}

CAF_MAIN()