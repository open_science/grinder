//  Created by Mikhail Kalugin on 07 / 09 / 2020.
//
// This file is a part of Siglab project
//
// Grinder - Signal data processing and delvery server.
//
// Copyright(C) 2020 Mikhail Kalugin.
//
// This program is free software : you can redistribute it and /or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see < https://www.gnu.org/licenses/>.

#pragma once
#include <caf/all.hpp>

class FlightRegistry
{
public:
    int32_t value;
};

using filghtRegistry = caf::typed_actor<caf::reacts_to<caf::put_atom, int32_t>,
    caf::replies_to<caf::get_atom>::with<int32_t>>;


filghtRegistry::behavior_type filghtRegistry_fun(filghtRegistry::stateful_pointer<FlightRegistry> self)
{
    return {
      [=](caf::put_atom, int32_t val) { self->state.value = val; },
      [=](caf::get_atom) { return self->state.value; },
    };
}