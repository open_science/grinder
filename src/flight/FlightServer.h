//  Created by Mikhail Kalugin on 11 / 02 / 2019.
//  Copyright 2019 Mikhail Kalugin All rights reserved.
//
// This file is a part of Siglab project
//
// Grinder - Signal data processing and delvery server.
//
// Copyright(C) 2019 Mikhail Kalugin.
//
// This program is free software : you can redistribute it and /or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see < https://www.gnu.org/licenses/>.

/// FlightServer.cpp  \brief Arrow Flight server implementation

#pragma once
#include <arrow/flight/server.h>
#include <caf/actor_system.hpp>
#include <caf/event_based_actor.hpp>
#include <caf/blocking_actor.hpp>

//namespace flt = ;

/// FlightServer
class FlightServer :
    public  arrow::flight::FlightServerBase
{
    caf::blocking_actor *owner;
    const caf::actor &logger;

public:
    FlightServer(caf::blocking_actor* owner, const caf::actor& logger);

    /// List of datasets what can be acquired from this server
    arrow::Status ListFlights(const arrow::flight::ServerCallContext& context,
        const arrow::flight::Criteria* criteria,
        std::unique_ptr <arrow::flight::FlightListing >* listings);

    /// Get info about dataset
    arrow::Status GetFlightInfo(
        const arrow::flight::ServerCallContext& context,
        const arrow::flight::FlightDescriptor& request,
        std::unique_ptr<arrow::flight::FlightInfo>* info);

    /// Get dataset
    arrow::Status DoGet(const arrow::flight::ServerCallContext& context, const
        arrow::flight::Ticket& request,
        std::unique_ptr<arrow::flight::FlightDataStream>* data_stream);

    /// Put dataset
    arrow::Status DoPut(const arrow::flight::ServerCallContext& context,
        std::unique_ptr<arrow::flight::FlightMessageReader> reader,
        std::unique_ptr<arrow::flight::FlightMetadataWriter> writer);

    /// Do action
    arrow::Status DoAction(const arrow::flight::ServerCallContext& context,
        const arrow::flight::Action& action,
        std::unique_ptr<arrow::flight::ResultStream>* result);

    /// List of possible actions
    arrow::Status ListActions(const arrow::flight::ServerCallContext& context,
        std::vector<arrow::flight::ActionType>* actions);

};
