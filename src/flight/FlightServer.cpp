// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//  Created by Mikhail Kalugin on 11 / 02 / 2019.
//
// This file is a part of Siglab project
//
// Grinder - Signal data processing and delvery server.
//
// Copyright(C) 2019 Mikhail Kalugin.
//
// This program is free software : you can redistribute it and /or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see < https://www.gnu.org/licenses/>.

/// FlightServer.cpp  \brief Arrow Flight server implementation

// �� ������ ����� �� �������� ��� ��������� ������ ������� ���������� �
// �������� �� �� ��� ������ � ������ ������.
// �������� � ���, ��� ������ ������� ������������ ������������� ��������.
// API FlightServerBase ���������� �� ����� �������, �������� ����� ������
// ���������� �� ������������� ������� ��� ������� � ������� � ������ �����.
//
// ������������� ����� �������� ��� ������:
// RegistryActor - �������� �� ��������� ���������.
// StorageActor - �������� �� ��������� � ���������� ������.
// ExecutorActor - �������� �� ���������� ��������.


// ������ ���� - �������������� ��������������. ���� ��� ������� (
// �� ������� ������� - ����� ����������� MVP)

// MVP - ������ �������� KvaserMemorator (�� ������ ����� - ��� ��������) ��
// �������� �������.
//
// ��������� �����:
// 1. ��������� �������. �������.
// 2. ������ ������ �� HDFS.
// 3. ���������� ������.
// 4. �������� �� ������� ������� (python, Grandiva)

// At this stage, it�s not obvious how to apply the actor model effectively and
// whether this is possible at all in this case.
// The problem is that the actor model assumes asynchronous operations.
// FlightServerBase API is synchronous in nature, possibly worth it at all
// refuse to use actors as such and go on with the task model.
//
// Conceptually, three actors can be distinguished:
// RegistryActor - responsible for receiving listings.
// StorageActor - is responsible for receiving and placing data.
// ExecutorActor - responsible for the execution of actions.


// The first stage is conceptual design. So far without actors (
// not enough time to implement MVP with actors)

// MVP - reading KvaserMemorator logs (at this stage - without plugins) from
// file system.
//
// The following steps:
// 1. Modular system. Plugins
// 2. Reading data from HDFS.
// 3. Adding data.
// 4. Server-side operations (python, Grandiva)

#include "config.h"
#include "FlightServer.h"

FlightServer::FlightServer(caf::blocking_actor *owner, const caf::actor &logger) :
    owner(owner),
    logger(logger)
{

}

arrow::Status FlightServer::ListFlights(
    const arrow::flight::ServerCallContext& context,
    const arrow::flight::Criteria* criteria,
    std::unique_ptr<arrow::flight::FlightListing>* listings)
{
    return arrow::Status::NotImplemented("NIY");
}

arrow::Status FlightServer::GetFlightInfo(
    const arrow::flight::ServerCallContext& context,
    const arrow::flight::FlightDescriptor& request,
    std::unique_ptr<arrow::flight::FlightInfo>* info)
{
    return arrow::Status::NotImplemented("NIY");
}

arrow::Status FlightServer::DoGet(
    const arrow::flight::ServerCallContext& context,
    const arrow::flight::Ticket& request,
    std::unique_ptr<arrow::flight::FlightDataStream>* data_stream)
{
    return arrow::Status::NotImplemented("NIY");
}

arrow::Status FlightServer::DoPut(
    const arrow::flight::ServerCallContext& context,
    std::unique_ptr<arrow::flight::FlightMessageReader> reader,
    std::unique_ptr<arrow::flight::FlightMetadataWriter> writer)
{
    return arrow::Status::NotImplemented("NIY");
}

arrow::Status FlightServer::DoAction(
    const arrow::flight::ServerCallContext& context,
    const arrow::flight::Action& action,
    std::unique_ptr<arrow::flight::ResultStream>* result)
{
    return arrow::Status::NotImplemented("NIY");
}

arrow::Status FlightServer::ListActions(
    const arrow::flight::ServerCallContext& context,
    std::vector<arrow::flight::ActionType>* actions)
{
    return arrow::Status::NotImplemented("NIY");
}
