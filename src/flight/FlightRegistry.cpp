// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//  Created by Mikhail Kalugin on 07 / 09 / 2020.
//  Copyright 2019 Mikhail Kalugin All rights reserved.
//
// This file is a part of Siglab project
//
// Grinder - Signal data processing and delvery server.
//
// Copyright(C) 2019 Mikhail Kalugin.
//
// This program is free software : you can redistribute it and /or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see < https://www.gnu.org/licenses/>.

#include "FlightRegistry.h"
